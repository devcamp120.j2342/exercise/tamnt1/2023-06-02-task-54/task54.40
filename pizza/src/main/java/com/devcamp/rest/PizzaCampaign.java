package com.devcamp.rest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PizzaCampaign {
    @CrossOrigin
    @GetMapping("/dev-date")
    public String getDayViet(@RequestParam(value = "name", defaultValue = "pizza lover") String name) {
        LocalDate currentDate = LocalDate.now();
        String formattedDate = currentDate
                .format(DateTimeFormatter.ofPattern("'Hôm nay 'EEEE', ngày 'dd' tháng 'MM' năm 'yyyy"));
        String message = "Hello " + name + "! " + formattedDate + ", mua 1 tặng 1.";
        return message;
    }
    @GetMapping("/greet")
    public String greetUser(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "firstname", defaultValue = "") String firstName,
            @RequestParam(value = "lastname", defaultValue = "") String lastName
    ) {
        int luckyNumber = generateLuckyNumber();
        String message = "Xin chào, " + username + "! Số may mắn hôm nay của bạn là: " + luckyNumber + ".";
        return message;
    }

    private int generateLuckyNumber() {
        Random random = new Random();
        return random.nextInt(6) + 1;
    }
}
